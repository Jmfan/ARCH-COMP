function [Rout,Rout_tp,res,tVec] = reach(obj,params,options)
% reach - computes the reachable set for linear system
%
% Syntax:  
%    [Rout,Rout_tp,res] = reach(obj,options)
%
% Inputs:
%    obj - continuous system object
%    params - model parameters
%    options - options for the computation of reachable sets
%
% Outputs:
%    Rout - reachable set of time intervals for the continuous dynamics
%    Rout_tp - reachable set of time points for the continuous dynamics
%    res  - boolean (only if property checked)
%    tVec - vector of time steps (only adaptive)
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:        Mark Wetzlinger
% Written:       26-June-2019
% Last update:   08-Oct-2019
%                23-April-2020 (restructure params/options)
% Last revision: ---


%------------- BEGIN CODE --------------

% options preprocessing
options = params2options(params,options);
options = checkOptionsReach(obj,options);

%if a trajectory should be tracked
if isfield(options,'uTransVec')
    options.uTrans = options.uTransVec(:,1);
end

%time period
tVec = options.tStart:options.timeStep:options.tFinal;

% initialize parameter for the output equation
[C,D,k] = initOutputEquation(obj,options);
Rout = cell(length(tVec)-1,1);
Rout_tp = cell(length(tVec)-1,1);

% initialize input set
Uadd = obj.B*(options.U + options.uTrans);

% initialize reachable set
Rnext.tp = options.R0;

% loop over all reachability steps
for i = 2:length(tVec)-1

    % compute output set
    Rout{i-1} = outputSet(C,D,k,Rnext,options);
    
    % safety property check
    if isfield(options,'specification')
        if inUnsafeSet(Rout{i-1},options.specification)
            % violation
            Rout = Rout(1:i-1);
            res = false;
            return
        end
    end
    
    
    %% post
    
    % if a trajectory should be tracked
    if isfield(options,'uTransVec')
        options.uTrans = options.uTransVec(:,i);
        
        % update input set
        Uadd = obj.B*(options.U + options.uTrans);
    end
    
    

    %write results to reachable set struct Rnext
    if isempty(obj.c)
        Rnext.tp = obj.A*Rnext.tp + Uadd;
    else
        Rnext.tp = obj.A*Rnext.tp + Uadd + obj.c;
    end
    Rnext.ti = []; %discrete time model

end

Rout{end} = outputSet(C,D,k,Rnext,options);

% safety property check
if isfield(options,'specification')
    if inUnsafeSet(Rout{end},options.specification)
        % violation, but no reduction in cell size of Rout, Rout_tp
        res = false;
        return
    end
end

res = true;


end


%------------- END OF CODE --------------