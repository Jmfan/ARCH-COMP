#!/usr/bin/env python

from common import mono_verify

import datetime

from resources.acc.acc_env import AccEnv
from resources.acc.acc_agent import AccAgent

from src.network_parser.network_model import NetworkModel
from src.utils.formula import *
from src.verification.bounds.bounds import HyperRectangleBounds


def main():

    # define some constants
    T_gap = 1.4
    D_default = 10.0

    agent, env = initialise_and_get_agent_and_env()
    #
    # Constraint specific variables of the initial state to one value by setting the upper
    # bounds equal to the lower bounds.

    # hardcoding values here. assuming variable order [x1, ... , x6]
    unzipped = zip(*[(90, 110), (32,32.2), (0,0), (10,11), (30,30.2), (0,0)])

    input_hyper_rectangle = HyperRectangleBounds(*unzipped)
    print(input_hyper_rectangle)

    # we are required to check if controller reaches bas state in 5s with a dt of 0.1s
    steps = range(1,51)

    for num_steps in steps:
        print(num_steps, "steps")

        # Define a linear expression encoding safety spec
        linexpr = LinearExpression({0: 1, 3: -1, 4: -1*T_gap})
        safe = LinExprConstraint(linexpr, GT, D_default)

        formula = ANextFormula(num_steps, safe)

        # Run verificaiton method.
        # Monotlithic encoding.
        log_info = mono_verify(formula, input_hyper_rectangle, agent, env)
        print("\n")

        with open("arch-comp.log", "a") as file:
            file.write(f"{datetime.datetime.now()}, ACC, {num_steps}, {log_info[0]:9.6f}, {log_info[1]}\n")



def initialise_and_get_agent_and_env():
    """
    Initialise agent and environment.
    :return: Initialised NeuralAgent and AccEnv objects.
    """

    x_square_model = NetworkModel()
    x_square_model.parse("../resources/acc/models/x_square.h5")

    controller_model = NetworkModel()
    controller_model.parse("../resources/acc/models/controller_5_20.h5")

    agent = AccAgent(controller_model)
    env = AccEnv(x_square_model)

    return agent, env


if __name__ == "__main__":
    main()
