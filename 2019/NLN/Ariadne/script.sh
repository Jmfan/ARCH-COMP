#/bin/sh
 
rm -Rf result
mkdir result
docker build -t ariadne .
export RUN_CMD="docker run -v $PWD/result:/mounted_result ariadne sh -c"
$RUN_CMD 'examples/continuous/vanderpol 2>&1 | tee vanderpol.log; cp vanderpol.* /mounted_result'
$RUN_CMD 'experimental/examples/continuous/laub-loomis-nonoise 2>&1 | tee laubloomis.log; cp laubloomis.* /mounted_result'
$RUN_CMD 'experimental/examples/continuous/quadrotor 2>&1 | tee quadrotor.log; cp quadrotor.* /mounted_result'
$RUN_CMD 'experimental/examples/hybrid/space_rendezvous 2>&1 | tee spacerendezvous.log; cp spacerendezvous.* /mounted_result'
